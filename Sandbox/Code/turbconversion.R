turbs3 %>%
  group_by(variable) %>%
  summarise(., numSite = n_distinct(Site)) %>%
  ggplot(aes(variable, numSite)) +
    geom_bar(stat = 'identity')

turbs3 %>%
  group_by_(.dots = c('Year', 'variable')) %>%
  filter(Year >= 2000) %>%
  summarise(., nvar = n_distinct(variable)) %>%
  ggplot(aes(Year, nvar, fill = variable)) +
    geom_bar(stat = 'identity')

turbs3 = subset(turbs3, turbs3$variable != 'Sld.sus.500c.mg.l')

susp = turbs3[which(turbs3$variable == 'Sld.sus.105c.mg.l'),]
ntu = turbs3[which(turbs3$variable == 'Turbidity'),]

#finding overlapping locations
overlap = subset(turbs3, turbs3$GenSite %in% intersect(susp$GenSite, ntu$GenSite))

ggplot(overlap2, aes(as.Date(Date...Time, format = "%d/%m/%Y %H:%M"), value, color = variable)) +
  geom_point() +
  facet_grid(GenSite ~., scales = 'free_y') +
  scale_x_date()

susp2 = overlap[which(overlap$variable == 'Sld.sus.105c.mg.l'),]
ntu2 = overlap[which(overlap$variable == 'Turbidity'),]

# finding overlapping measurement times from overlapping locations
overlap2 = merge(susp2, ntu2, by = c('GenSite', 'Date...Time', 'Year', 'Month', 'Category', 'Site'))
colnames(overlap2) = c('GenSite', 'Date...Time', 'Year', 'Month', 'Category', 'Site', 'variable.x', 'SusSol', 'variable.y', 'NTU')

# getting data on sites that can be converted from NTU to Suspended Solids
oversites = subset(turbs3, turbs3$GenSite %in% unique(overlap2$GenSite))

#doing regression analysis to get equation for conversion
regres = overlap2 %>%
  group_by(GenSite) %>%
  do(mod = lm(SusSol ~ NTU, data = .)) %>%
  mutate(slope = coef(mod)[2]) %>%
  mutate(intercept = coef(mod)[1]) %>%
  mutate(r2 = summary(mod)$r.squared) %>%
  distinct(GenSite, .keep_all = T)

oversites2 = merge(oversites, regres, by = 'GenSite')


convert = function(data){
  
  tbc = data[which(data$variable == 'Turbidity'),]
  
  tbc$SusSol = tbc$intercept + tbc$slope*tbc$value

  return(tbc)
  
}

test = oversites2 %>%
  group_by(GenSite) %>%
  do(., t = convert(.))

test = unnest(test) 
test2 = test %>%
  select(-c(GenSite1, mod, slope, intercept, value)) %>%
  rename(., value = SusSol)

test2$variable = sub("Turbidity", "SuspSol", test2$variable, ignore.case = T)


# regressconvert = function(data){
#   
#   mod = lm(SusSol ~ NTU, data)
# 
#   SS = coef(mod)[1] + coef(mod)[2]*data$NTU
#   
#   out = data.frame(data$NTU, SS)
#   colnames(out) = c('NTU', 'SS')
#   
#   return(out)
# }
# 
# 
# t2 = overlap2 %>%
#   group_by(GenSite) %>%
#   do(., t = regressconvert(.))
# 
# t3 = t2 %>%
#   unnest(t)
# 
# # plotting each GenSite with equation and R2 value
# lm_eqn <- function(df){
#   m <- lm(SusSol ~ NTU, df);
#   eq <- substitute(italic(y) == a + b %.% italic(x)*","~~italic(r)^2~"="~r2, 
#                    list(a = format(coef(m)[1], digits = 2),
#                         b = format(coef(m)[2], digits = 2),
#                         r2 = format(summary(m)$r.squared, digits = 3)))
#   as.character(as.expression(eq));
# }
# 
# plotthing = function(data){
#   
#   g = ggplot(data, aes(NTU, SusSol)) +
#     geom_point() +
#     geom_smooth(method = 'lm', se = F) +
#     geom_text(x = min(data$NTU) + 200, y = max(data$SusSol), label = lm_eqn(data), parse = TRUE)
# 
#   print(g)  
# }
# 
# overlap2 %>%
#   group_by(GenSite) %>%
#   do(., t = plotthing(.))
# 
# 
# # not Normal, still not Normal when logged
# overlap2 %>%
#   group_by(GenSite) %>%
#   summarise(., thing = shapiro.test(log(SusSol))$p.value)
# 
# 
# ggplot(predvals, aes(NTU, SS)) +
#   geom_line() +
#   geom_point(data = test1, aes(NTU, SusSol))
