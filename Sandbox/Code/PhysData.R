# Author: Julia Lanoue (j.lanoue@ucl.ac.uk)
# Date Created: 10 Oct 2018
# Description: Data standardization and analysis for non-EA physical parameter datasets. All statistical analyses between the non-EA and EA datasets, such as correlation and regressions.

# importing packages
require(dplyr)
require(tidyr)
require(ggplot2)
require(reshape2)
require(cowplot)
require(stringr)
require(Kendall)
require(car)
require(pls)

# importing scripts
source("quicklook.R")

# Thames21 Water Quality ---------------------------------------------------
waterdata = read.csv("../Data/RawData/Water Quality raw.csv")

waterdata = subset(waterdata, select = c(Created..Date, Location..Latitude, Location..Longitude, Dissolved.Oxygen..ppm, Temperature..C, Turbidity, Location..NGR))

colnames(waterdata) = c('Date', 'Latitude', 'Longitude', 'DO', 'TempC', 'Turbidity', 'NGR')

waterdata$Date = as.Date(waterdata$Date, format = '%d/%m/%Y')

# majority of data (74% is NA, can't places into spatial categories)
ggplot(waterdata, aes(Date, DO)) +
  geom_point() +
  scale_x_date()

# Mean Air Temp (C) -----------------------------------------------------------
airtemp = read.table("../Data/airtemp.txt", sep = "\t", header = T)

# mean annual air temperature 
air = ggplot(airtemp[which(airtemp$Year >= 1992),], aes(Year, ANN)) +
  geom_point() +
  ylab('Average Air Temp (C)') +
  geom_smooth(method = 'lm', se = F) +
  theme_bw()

# R2 = 0.37  
summary(lm(ANN ~ Year, data = airtemp))


# NAO Index ------------------------------------------------------------------
nao = read.table("../Data/nao.txt", sep = "\t", header = T)

nao = melt(nao, id.var = 'X')

colnames(nao) = c('Year', 'Month', 'value')
nao$Month = match(factor(nao$Month, labels=month.abb), month.abb)
nao$value = replace(nao$value, nao$value == -999., NA)
nao$MonYear = as.Date(paste(01, nao$Month, nao$Year, sep="/"), 
                      format = '%d/%m/%Y')


naoAn = nao %>%
  group_by(Year) %>%
  summarise(., avg = mean(value))

ggplot(naoAn, aes(Year, avg)) +
  geom_bar(stat = 'identity')

nao %>%
  mutate(MonYear = as.Date(paste(01, Month, Year, sep="/"), 
                           format = '%d/%m/%Y')) %>%
  ggplot(aes(MonYear, value)) +
    geom_bar(stat = 'identity')
  
# using temp data loaded in from PhysCOllation.R

MonTemp = temp %>%
  group_by_(.dots = c('Month', 'Year')) %>%
  summarise(., avgs = mean(value)) %>%
  mutate(MonYear = as.Date(paste(01, Month, Year, sep="/"), 
                           format = '%d/%m/%Y'))

tnao = merge(nao, MonTemp, by = 'MonYear')

# scatterplot - basically a cloud :(
ggplot(tnao, aes(value, avgs)) +
  geom_point() +
  geom_smooth()

# not significant
Kendall(tnao$value, tnao$avgs)

# Rainfall ----------------------------------------------------------------
rain = read.table("../Data/Unused_Data/precipitation.txt", header = T)
colnames(rain) = str_to_title(names(rain))
rainrel = subset(rain, rain$Year >= 2000, select = c(Year:Dec))

rainrel = melt(rainrel, id.vars = 'Year')
rainrel = rainrel[which(rainrel$value > 0), ] 
rainrel$variable = match(factor(rainrel$variable, labels=month.abb), month.abb)
colnames(rainrel) = c('Year', 'Month', 'rain.mm')
rainrel$MonYear = as.Date(paste(01, rainrel$Month, rainrel$Year, sep="/"), 
        format = '%d/%m/%Y')

rainnao = merge(nao, rainrel, by = 'MonYear')

ggplot(rainnao, aes(value, rain.mm)) +
  geom_point() +
  geom_smooth(method = 'lm')

Kendall(rainnao$value, rainnao$rain.mm)
cor.test(rainnao$value, rainnao$rain.mm, method = 'kendall')

rainrel %>%
  group_by(Year) %>%
  summarise(., avgs = mean(value), sd = sd(value)) %>%
  # mutate(MonYear = paste(01, Month, Year, sep="/" )) %>%
  ggplot(aes(Year, avgs)) +
    geom_point() +
    # scale_x_date() +
    geom_errorbar(aes(ymin = avgs - sd, ymax = avgs + sd))
    


precip = quicklook(rainrel, "Precipitation (mm)")
ggsave(precip, filename = "../Graphs/rainfall.pdf")
# physcomb = plot_grid(air, naoplot, rainplot)
# ggsave(physcomb, filename = '../Graphs/PhysCom.pdf')


# SST ---------------------------------------------------------------------
# fit shows temp constant/declining over time

leigh = read.table('../Data/leighonsea.txt', sep = "\t", header = T)
lb = read.table('../Data/littlebrook.txt', sep = '\t', header = T)
til = read.table('../Data/tilbury.txt', sep = '\t', header = T)

leigh2 = melt(leigh, id = 'Year')
leigh2$GenSite = 'leigh on sea'

lb2 = melt(lb, id = 'Year')
lb2$GenSite = 'little brook'

til2 = melt(til, id = 'Year')
til2$GenSite = 'tilbury'

sst = rbind(leigh2, lb2, til2)
sst$value = as.numeric(sst$value)
sst = na.omit(sst)

# adding Lat/Long columns
leigh$Latitude = 51.533
leigh$Longitude = 0.650

lb$Latitude = 51.466
lb$Longitude = 0.250

til$Latitude = 51.450
til$Longitude = 0.400

# calculating average temp for each year - 110/139 have missing values 
sst %>%
  group_by_(.dots = c('GenSite', 'Year')) %>%
  summarise(., avgs = mean(value, na.rm = T), sd = sd(value, na.rm = T)) %>%
  ggplot(aes(Year, avgs)) +
    geom_point() +
    geom_errorbar(aes(ymin=avgs-sd, ymax=avgs+sd), width=.2,
                  position=position_dodge(.9)) +
    facet_wrap(~GenSite, ncol = 1) +
    geom_smooth(method = 'lm')

ggplot(sst, aes(Year, color = variable)) +
    geom_histogram() +
    facet_wrap(~GenSite, ncol = 1)

ggplot(sst, aes(Year, Jun)) +
  geom_point() +
  facet_grid(Latitude~.) +
  theme_bw() +
  geom_smooth(method = 'lm')
  # geom_line() +
  # xlim(1992, 2010)




# Gauged Flow (Kingston on Thames) ----------------------------------------

flow = read.csv("../Data/RawData/39001_gdf.csv")

flow = flow[-(1:20),]

names(flow) = c('Date', 'Flow.m3s', 'Notes')

flow$Date = as.Date(flow$Date, format = '%Y-%m-%d')
flow$Month = format(as.Date(flow$Date, format="%Y-%m-%d"),"%m")
flow$Year = format(as.Date(flow$Date, format="%Y-%m-%d"),"%Y")

flowsub = subset(flow, flow$Year >= 2000)
flowsub$Flow.m3s = as.numeric(levels(flowsub$Flow.m3s)[flowsub$Flow.m3s])

p = flowsub %>%
  group_by_(.dots = c('Month','Year')) %>%
  summarise(., mean = mean(Flow.m3s), sd = sd(Flow.m3s)) %>%
  mutate(Date = as.Date(paste(01, Month, Year, sep="/"), format = '%d/%m/%Y')) %>%
  ggplot(aes(Date, mean)) +
    geom_point(color = "#999999") +
    geom_errorbar(aes(ymin=mean-sd, ymax=mean+sd), width=.1,
                  position=position_dodge(.9), color = "#999999") +
    theme_bw() +
    scale_x_date() +
    # theme(axis.text.x = element_text(angle = 45)) +
    # scale_x_discrete(breaks = c(2000, 2005, 2010, 2015)) +
    ylab(expression("Gauged Freshwater Flow ("~m^{3}~"/s)")) +
    geom_smooth(se = T, color = 'blue', fill = 'black') +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(), 
          panel.border = element_blank(), 
          axis.line = element_line(color = 'black'))
print(p)

ggsave(p, filename = "../Graphs/FreshFlow3.pdf")

flowts = ts(data = flowsub$Flow.m3s, start = c(2000, 1), end = c(2017, 4), frequency = 12)

SeasonalMannKendall(flowts)

test = test %>%
  group_by(Year) %>%
  summarise(., mean = mean(value), sd = sd(value))

q = ggplot(test, aes(Year, mean)) +
  geom_point(col = 'blue') +
  geom_errorbar(aes(ymin=mean-sd, ymax=mean+sd), width=.2,
                position=position_dodge(.9)) +
  theme_bw() +
  ylab('Avg Annual Salinity @ London Bridge')

flow = plot_grid(p, q, nrow = 2)

ggsave(flow, filename = "../Graphs/freshsalcompare.pdf")


# Correlation Analysis ----------------------------------------------------

# SALINITY AND FRESHWATER INPUT 
# pooled salinity data needs to be loaded from PhysCollation.R script


sal2 = salinity %>%
    group_by(Category) %>%
    filter(n_distinct(Year) >= 5) %>%
    filter(!(abs(value - mean(value)) > 2*sd(value))) %>%
    group_by_(.dots = c('Category', 'Month', 'Year')) %>%
    summarise(., meanSal = mean(value))

flowsub2 = flowsub %>%
  group_by_(.dots = c('Month', 'Year')) %>%
  summarise(., meanFlow = mean(Flow.m3s))

# normally distributed
shapiro.test(flowsub2$meanFlow)[2]

salnorm = sal2 %>%
  group_by(GenSite) %>%
  summarise(statistic = shapiro.test(mean)$statistic,
            p.value = shapiro.test(mean)$p.value)

# 10 sites with non-normal distribution
sum(salnorm$p.value < 0.05)

SalFlow = merge(sal2, flowsub2, by = c('Year', 'Month'))

# scatter plot
salflowscat = ggplot(SalFlow, aes(meanFlow, meanSal)) +
  geom_point(show.legend = F, color = "#999999") +
  facet_grid(Category~., scales = 'free_y') +
  theme(legend.position = 'none') +
  theme_bw() +
  geom_smooth(se = F, show.legend = F, color = 'blue') +
  xlab(expression("Gauged Freshwater Flow ("~m^{3}~"/s)")) + 
  ylab('Salinity (ppt)') +
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank())
print(salflowscat)
ggsave(salflowscat, filename = "../Graphs/AllDataGraphs/AllSalFlowScat.pdf")

# using Kendall tau-b to handle tied data
tau = SalFlow %>%
  group_by(Category) %>%
  summarise(KenPval = Kendall(meanFlow, meanSal)$sl[1], 
            KenTau = Kendall(meanFlow, meanSal)$tau[1])

# merging with original dataset
SalFlow = merge(SalFlow, stat, by = 'Category')

ggplot(SalFlow[which(SalFlow$Category == 'fresh'),], 
       aes(meanFlow, meanSal)) +
  geom_point() +
  facet_wrap(~GenSite, scales = 'free_y') +
  geom_smooth(se = F) +
  theme_bw()

SalFlow %>%
  group_by(Category) %>%
  summarise(., mean = mean(KenTau), median = median(KenTau), sd = sd(KenTau))


# TEMPERATURE AND NAO INDEX
# get temperature data from PhyCollation 

temp2 = temp %>%
  group_by_(.dots = c('Year', 'Month', 'Category')) %>%
  summarise(., avgs = mean(value), sd = sd(value)) %>%
  mutate(MonYear = paste(01, Month, Year, sep="/" )) %>%
  rename(., tempC = avgs)

nao$MonYear = as.Date(nao$MonYear, format = '%d/%m/%Y')
temp2$MonYear = as.Date(temp2$MonYear, format = '%d/%m/%Y')

naotemp = merge(nao, temp2, by = c('MonYear', 'Month', 'Year'))

ggplot(naotemp, aes(value, tempC)) +
  geom_point()

Kendall(naotemp$value, naotemp$tempC)

tauNT = naotemp %>%
  group_by(Year) %>%
  summarise(KenPval = Kendall(value, tempC)$sl[1], 
            KenTau = Kendall(value, tempC)$tau[1])

# not doing R2 with pooled data because salinity data and monthly rainfall not Normally distributed

# # doing linear model to get amount of variation explained by flow on salinity
# statlm = SalFlow %>%
#   group_by(GenSite) %>%
#   summarise(., lmPval = summary(lm(meanSal ~ meanFlow))[8])
# 
# statlm$lmPval = unlist(statlm$lmPval)
# 
# # merging R2 values with data set
# SalFlow = merge(SalFlow, statlm, by = 'GenSite')
# 
# # checking means 
# SalFlow %>%
#   group_by(Category) %>%
#   summarise(., meanlm = mean(lmPval), meanTau = mean(KenTau))
# 
# # looking at distinct sites to see if patterns emerge of R2/tau value and location
# SalFlow2 = SalFlow %>%
#   distinct(GenSite, .keep_all = T) %>%
#   arrange(., lmPval, KenTau, Category)


# FRESHWATER INPUT AND RAINFALL

# not significant (dist is Normal)
shapiro.test(flowsub2$meanFlow)[2]

# calculating annual means for rainfall
rainmean = rainrel %>%
  group_by(Year) %>%
  filter(Year >= 1997) %>%
  filter(Year < 2018) %>%
  summarise(., meanRain = mean(value))

# not significant (dist is Normal)
shapiro.test(rainmean$meanRain)[2]

# significant, positively correlated
cor.test(flowsub2$meanFlow, rainmean$meanRain, method = 'pearson')

FlowRain = merge(flowsub2, rainmean, by = 'Year')

ggplot(FlowRain, aes(meanRain, meanFlow)) +
  geom_point() +
  theme_bw()

# R2 = 0.71
summary(lm(meanFlow ~ meanRain, data = FlowRain))[8]

big = merge(FlowRain, SalFlow, by = 'Year')

test = big %>%
  group_by(Category) %>%
  do(., thing = summary(lm(meanSal ~ meanRain + meanFlow.x, data = .)))

test$thing = unlist(test$thing)

# using car package to calculated VIF
vif(lm(meanSal ~ meanRain + meanFlow.x, data = big))

