Grid	British National Grid
Datum	Ord Srvy Grt Britn

Header	Name	Description	Type	Position	Altitude	Depth	Proximity	Temperature	Display Mode	Color	Symbol	Facility	City	State	Country	Date Modified	Link	Categories

Waypoint	164	16-AUG-17 9:01:17	User Waypoint	TQ 53696 79148	4 m				Symbol & Name	Black	Beach							
Waypoint	165	16-AUG-17 9:13:16	User Waypoint	TQ 53697 79144	4 m				Symbol & Name	Black	Beach							
Waypoint	166	16-AUG-17 9:21:07	User Waypoint	TQ 53696 79149	6 m				Symbol & Name	Black	Beach							
Waypoint	167	16-AUG-17 9:29:43	User Waypoint	TQ 53696 79142	7 m				Symbol & Name	Black	Beach							
Waypoint	168	16-AUG-17 9:32:33	User Waypoint	TQ 53695 79137	4 m				Symbol & Name	Black	Beach							
Waypoint	169	16-AUG-17 9:49:47	User Waypoint	TQ 53695 79132	7 m				Symbol & Name	Black	Beach							
Waypoint	170	16-AUG-17 9:52:24	User Waypoint	TQ 53694 79129	5 m				Symbol & Name	Black	Beach							
Waypoint	171	16-AUG-17 10:02:58	User Waypoint	TQ 53688 79108	4 m				Symbol & Name	Black	Beach							
Waypoint	172	16-AUG-17 10:05:05	User Waypoint	TQ 53688 79105	1 m				Symbol & Name	Black	Beach							
Waypoint	173	16-AUG-17 10:10:42	User Waypoint	TQ 53687 79102	3 m				Symbol & Name	Black	Beach							
Waypoint	174	16-AUG-17 10:51:05	User Waypoint	TQ 53693 79115	5 m				Symbol & Name	Black	Beach							
Waypoint	175	16-AUG-17 10:57:07	User Waypoint	TQ 53706 79123	5 m				Symbol & Name	Black	Beach							

