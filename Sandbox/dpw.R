# Author: Julia Lanoue (j.lanoue@ucl.ac.uk)
# Date: Dec 12 2018
# Description: Standardization and combination of DPWorld data. Only 1m above sea level data used

require(dplyr)
require(reshape2)
require(ggplot2)
require(measurements)

# reading in the data
rl02 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data2.csv")
rl04 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data4.csv")
rl05 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data5.csv")
rl06 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data6.csv")
rl07 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data7.csv")
rl08 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data8.csv")
rl09 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data9.csv")
rl10 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data10.csv")
rl11 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data11.csv")
rl12 = read.csv("../Data/RawData/dpdata/DPW_RL_Baseline_TS_Data12.csv")

sr01 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data1.csv")
sr02 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data2.csv")
# sr03 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data3.csv")
sr04 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data4.csv")
sr05 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data5.csv")
sr06 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data6.csv")
sr07 = read.csv("../Data/RawData/dpdata/DPW_SR_Baseline_TS_Data7.csv")

# tes01 = read.csv("../Data/RawData/dpdata/DPW_TES_Baseline_TS_Data1.csv")
tes02 = read.csv("../Data/RawData/dpdata/DPW_TES_Baseline_TS_Data2.csv")
tes03 = read.csv("../Data/RawData/dpdata/DPW_TES_Baseline_TS_Data3.csv")

# creating dataframe with location info
x <- sapply(sapply(ls(), get), is.data.frame)
SiteCode = sort(names(x)[(x==TRUE)]) 

# creating table with original lat and long values
Loc = read.table(text = "
Latitude Longitude
51�29.53	000�28.66
51�29.87	000�30.77
51�30.00	000�33.89
51�30.31	000�36.60
51�29.83	000�39.92
51�29.93	000�43.11
51�29.11	000�46.92
51�30.06	000�49.01
51�29.16	000�50.01
51�29.53	000�56.67
51�26.67	000�23.70
51�30.03  000�27.50
51�30.46  000�31.70
51�30.71	000�33.06
51�31.16	000�39.60
51�32.01	000�51.09
51�30.080 000�32.530
51�30.066 000�42.526", header = T)

# converting coordinates to decimal
Loc$Latitude = gsub('�', ' ', Loc$Latitude)
Loc$Longitude = gsub('�', ' ', Loc$Longitude)

Loc$Latitude = measurements::conv_unit(
  Loc$Latitude, from = 'deg_dec_min', to = 'dec_deg')
Loc$Longitude = measurements::conv_unit(
  Loc$Longitude, from = 'deg_dec_min', to = 'dec_deg')

# finding standardised names to coordinates
Loc$GenSite = c('blyth sands', 'blyth sands', 'blyth sands', 'mouth', 'mouth', 'mouth', 'mouth', 'mouth', 'mouth', 'mouth', 'gravesend', 'standford le hope', 'coryton', 'canvey island', 'southend on sea', 'foulness', 'canvey island', 'southend on sea')

# adding column with original site names
Loc$SiteCode = SiteCode

# function to combine and standardise all dataframes
combine = function(...){
  dfs = list(...)
  
  dfs2 = Map(cbind, dfs, SiteCode = SiteCode)
  
  dfs3 = melt(dfs2, id.vars = c('SiteCode', 'DateTime'))
  
  dfs3 = merge(dfs3, Loc, by = 'SiteCode')
  
  dfs3 = select(dfs3, -L1)
  
  return(dfs3)
}

alldpw = combine(rl02, rl04, rl05, rl06, rl07, rl08, rl09, rl10, rl11, rl12, sr01, sr02, sr04, sr05, sr06, sr07, tes02, tes03)

# creating seperate date, month, and year column
alldpw$Date = as.Date(alldpw$DateTime, format = '%d/%m/%Y')
alldpw$Month = format(as.Date(alldpw$Date, format="%d/%m/%Y"),"%m")
alldpw$Year = format(as.Date(alldpw$Date, format="%d/%m/%Y"),"%Y")

alldpw$Category = 'marine'

# creating two seperate csv's because 32bit comp can't do it in one
alltemp = subset(alldpw, alldpw$variable == 'Temperature')
allsal = subset(alldpw, alldpw$variable == 'Salinity')

write.csv(alltemp, "../Data/dpwT.csv")
write.csv(allsal, "../Data/dpwS.csv")


# DPW Initial analysis ----------------------------------------------------

dpwSal = read.csv("../Data/dpwS.csv")
dpwTemp = read.csv("../Data/dpwT.csv")

# looking at change over time
dpwSal %>%
  group_by_(.dots = c('GenSite','Date')) %>%
  summarise(., dailyavgs = mean(value)) %>%
  ggplot(aes(as.Date(Date, format = '%Y-%m-%d'), dailyavgs)) +
    # geom_bar(stat = 'identity') +
    geom_point() +
    facet_wrap(~GenSite) +
    scale_x_date()
    # geom_smooth()
