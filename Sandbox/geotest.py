#!/usr/bin/env python3

import geopandas
import numpy as np
import pandas as pd
from shapely.geometry import Point

import missingno as msn

import seaborn as sns
import matplotlib.pyplot as plt

# loading in data
country = geopandas.read_file("../Data/gz_2010_us_040_00_5m.json")
# seeing first  rows of df
country.head()
# checking what type of df 
type(country)
type(country.geometry[0])

country.plot()
# plt.show()

country[country['NAME'].isin(['Alaska','Hawaii']) == False].plot(figsize=(30,20), color='#3B3C6E');

flo = pd.read_csv("../Data/florance.csv")
flo.head()
flo.info() # gives basic info on df, including any null and missing values

# visualizes missing data
msn.bar(flo, color = 'darkolivegreen');

# quick stats on df
flo.describe()

flo = flo.drop(['AdvisoryNumber','Forecaster','Received'], axis = 1)
flo.head()

# getting Longitude numbers correct
flo['Long'] = 0 - flo['Long']

#combining Long and Lat columns into one 
flo['coordinates'] = flo[['Long', 'Lat']].values.tolist()
# converting coordinates column to geoPoint
flo['coordinates'] = flo['coordinates'].apply(Point)

# converting to geopandas df
flo = geopandas.GeoDataFrame(flo, geometry = 'coordinates')
type(flo)
type(flo['coordinates'])

# subsetting data based on name, works same as regular pandas df
flo[flo['Name'] == 'Six']
# Counting number of names and types 
flo.groupby('Name').Type.count()

print "Mean wind speed of Hurricane Florence is {} mph and it can go up to {} mph maximum".format(round(flo.Wind.mean(),4), flo.Wind.max())

flo.plot(figsize = (20,10));

fig, ax = plt.subplots(1, figsize=(30,20))
base = country[country['NAME'].isin(['Alaska','Hawaii']) == False].plot(ax=ax, color='#3B3C6E')
flo.plot(ax=base, color='darkred', marker="*", markersize=10);

fig, ax = plt.subplots(1, figsize=(20,20))
base = country[country['NAME'].isin(['Alaska','Hawaii']) == False].plot(ax=ax, color='#3B3C6E')
flo.plot(ax=base, column='Wind', marker="<", markersize=10, cmap='cool', label="Wind speed(mph)")
_ = ax.axis('off')
plt.legend()
ax.set_title("Hurricane Florence in US Map", fontsize=25)
plt.savefig('Hurricane_footage.png',bbox_inches='tight');
