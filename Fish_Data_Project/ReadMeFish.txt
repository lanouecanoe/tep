Fish population monitoring on the Thames 

Overview: Project that investigated the quality and quantity of open access data on fish populations in the River Thames and Estuary. The study focused on determining if the dat a available can be used to track long term trends in fish populations over time. 

Code
clean_cefas.R
  R version 3.5.2
  Packages: ggplot2, dplyr
    - ggplot2 used for plotting 
    - dplyr used for basic counts and grouping
  Description: Script used to load in cefas data sets, collate them into a single data set that is saved as a new csv (cefas.csv). Exploratory plots and statistics are also done. 
  
fresh.R
  R version 3.5.2
  Packages: dplyr, ggplot2
   - dplyr used for subsetting 
   - ggplot2 used for basic visualization
  Description: Script used for subsetting freshwater fish data to meeting the minimum data standards set in the power analysis done in newfish.R 
  
newfish.R
  R version 3.5.2
  Packages: dplyr, ggplot2, multcompView, pwr, cowplot, pgirmess
   - dplyr used for grouping and running functions
   - ggplot2 is used for visualisation and plotting
   - multcompView is for visualising the results of the Kruskal-Wallis statistic in plots
   - pwr used to do power analysis 
   - cowplot is used to combine plots to produce one figure
   - pgirmess is used to perform the Kruskal-Wallis
  Description: Script that inputs the TRaC fish length data, subsets it to the relevant locations and timelines. A power analysis is performed to determine the minimum data needed per year to statistically analyze differences between years. Two Kruskal-Wallis analyses are done - one to investigate differences in body sizes between months within seasons and another to investigate differences in body sizes across time. A new file with the subsetted data (fishcorrdata.csv) is created for use in fishphys.R. 
  
fishphys.R
  R version 3.5.2
  Packages: dplyr, ggplot2, stringr, Kendall, pgirmess
   - dplyr is used for grouping
   - stringr is used for formatting species and site names
   - Kendall is used for correlations 
   - pgirmess is used for Kruskal-Wallis analysis 
  Description: Script that compares the fish length data subsetting from newfish.R and compares it to relevant physical data. The script contains several functions that create figures for comparing the two data sets to understand overlap and any correlations between trends in fish body size and physical parameters.
  
Data
trac.csv
fishcorrdata.csv
Freshwater_Data - directory of original freshwater data from data.gov.uk with documentation
  Freshwater Fish Counts for all species for all Areas and all Years_20190429.csv
  Freshwater_Fish_Counts_Datasets_Documentation.docx