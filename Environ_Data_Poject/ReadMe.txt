Environmental monitoring on the Thames 

Overview: Project investigating the quality and quantity of open access environmental data on the tidal Thames, focusing on temperature, salinity, dissolved oxygen, and turbidity levels since 2000. 

Code
EAfiles.R
  R version 3.5.2
  Packages: dplyr, ggplot2
    - dplyr used to group data for various calculations 
    - ggplot2 used for initial data visualization
  Description: Script used to load in, subset, and standardize the raw data files from the Environment Agency - the AQMS data and data provided via a query through the EA. The script uses Extraction.R and SiteStandardization.R to subset the relevant data and standardize sites. General sampling site names created from SiteStandardization.R script are initially categorized by salinity category using salinity data from the EA files, and a new csv - SiteCats.csv - with the general sampling site names and their corresponding salinity categories is created. EAphysdata.csv and aqmsAll.csv are created with the standardized format.
  
Extraction.R
  R version 3.5.2
  Packages: reshape2, stringr
    - reshape2 is used to melt the columns in the data set
    - stringr is used to standardize strings in columns 
  Description: The script contains a function that takes in x number of data sets, location names, and regular expression patterns to locate columns within each data set. The function returns a list of dataframes.

PhysCollation.R
  R version 3.5.2
  Packages: dplyr, ggplot2, stringr, cowplot, Kendall
    - dplyr is used for grouping the data 
    - ggplot2 is used for creating figures 
    - cowplot is used for creating figures
    - Kendall is used for the time series analysis 
    - stringr is to standardize strings in columns
  Description: A script collating and analyzing the environmental data that was cleaned and standardized in EAfiles.R and wimsEA.R. It runs the time series analysis and creates figures. Creates physcollated.csv - a file with all the environmental data, outliers removed.

SiteStandardization.R
  R version 3.5.2
  Packages: stringr
    - stringr is used to standardize general sampling site names
  Description: Script that contains two functions that create the general sampling site names. The input is the unstandardized column of site names and a vector of regular expressions with unwanted terms or symbols. The output is a vector with the standardized names.

wimsEA.R
  R version 3.5.2
  Packages: dplyr, ggplot2, reshape2
    - dplyr is used for grouping data
    - ggplot2 is used for initial data visualization but not essential for running the script
    - reshape2 is for melting the data
  Description: Script that loads in the raw data files from the WIMS data. It subsets the data to include only Thames-specific data. SiteStandardization.R is used to create the general sampling site names. wimsphys.csv is created with the standardized column names and format. SiteCats.csv is amended to include additional general sampling sites.
  
Data
aqmsAll.csv
EAphysdata.csv
SiteCats.csv - created in EAfiles.R script 
physcollated.csv
wimsphys.csv